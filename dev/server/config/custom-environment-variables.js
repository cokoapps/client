module.exports = {
  db: {
    host: 'POSTGRES_HOST',
    database: 'POSTGRES_DB',
    port: 'POSTGRES_PORT',
    user: 'POSTGRES_USER',
    password: 'POSTGRES_PASSWORD',
  },
}
